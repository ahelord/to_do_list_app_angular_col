import {TestBed, async} from '@angular/core/testing';

import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {UserService} from './services/user.service'
import {TaskService} from './services/task.service'

import {HttpModule} from '@angular/http';

describe('AppComponent', () => {
    let userServiceStub = {
        usersRespond: [{id_user: 1, full_name: 'Test User'}]
    };


    let taskServiceStub = {
        tasksRespond: [{id_task: 1, description: 'Hacer footer', id_user: 1}]
    };


    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, HttpModule],
            declarations: [
                AppComponent
            ],
            providers: [
                {provide: UserService, useValue: userServiceStub},
                {provide: TaskService, useValue: taskServiceStub}
            ]
        }).compileComponents();
    }));

    it('Puedo crear la aplicacion', async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));

    it('Puedo renderizar la etiqueta H1', async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        fixture.detectChanges();
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('h1').textContent).toContain('To-Do List App');
    }));
});
