/**
 * Created by lrodriguez on 6/4/17.
 */

import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';

import 'rxjs/add/operator/toPromise';

import {Task} from '../models/task.model';
import {ProxyApi} from '../models/proxy-api.model'
@Injectable()
export class TaskService {
  private proxyApi = new ProxyApi();
  private taskUrl = 'task';
  private headers = new Headers({'Content-Type': 'application/json'});


  constructor(private http: Http) {
  }


  getTasksByidUser(idUser: number): Promise<Task[]> {
    return this.http.get(this.proxyApi.proxy + this.taskUrl + '/' + idUser)
      .toPromise()
      .then(response => response.json() as Task[])
      .catch(this.handleError);
  }

  postTask(task: any): Promise<Task> {
    return this.http.post(this.proxyApi.proxy + this.taskUrl, task, {headers: this.headers})
      .toPromise()
      .then(response => response.json() as Task)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
