/**
 * Created by lrodriguez on 6/4/17.
 */

import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';

import 'rxjs/add/operator/toPromise';

import {User} from '../models/user.model';
import {ProxyApi} from '../models/proxy-api.model'
@Injectable()
export class UserService {
  private proxyApi = new ProxyApi();
  private userUrl = 'user';
  private headers = new Headers({'Content-Type': 'application/json'});


  constructor(private http: Http) {
  }


  getUsers(): Promise<User[]> {
    return this.http.get(this.proxyApi.proxy + this.userUrl)
      .toPromise()
      .then(response => response.json() as User[])
      .catch(this.handleError);
  }

  postUser(user: User): Promise<User> {
    return this.http.post(this.proxyApi.proxy + this.userUrl, user, {headers: this.headers})
      .toPromise()
      .then(response => response.json() as User)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
