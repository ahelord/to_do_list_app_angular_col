import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

/*Start Services*/
import {UserService} from "./services/user.service";
import {TaskService} from "./services/task.service";
/*End Services*/
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [UserService,TaskService],
  bootstrap: [AppComponent]
})
export class AppModule { }
