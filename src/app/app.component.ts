import {Component} from '@angular/core';
import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';

import 'rxjs/add/operator/toPromise';

/*Start Import Models*/
import {User} from './models/user.model'
import {Task} from './models/task.model'
/*End Import Models*/

/*Start Services*/
import {UserService} from "./services/user.service";
import {TaskService} from "./services/task.service";
/*End Services*/

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'app works!';
    user = new User();
    task = new Task();
    users = new Array<User>();
    tasks = new Array<Task>();
    idUserSelect: number;


    constructor(private userService: UserService,
                private taskService: TaskService) {
    }

    addUser(user: any) {
        this.userService.postUser(user)
            .then(userRespond => {
                this.user = userRespond;
                this.getUsers();
            })
    }

    getUsers() {
        this.userService.getUsers()
            .then(usersRespond => {
                this.users = usersRespond;
            })
    }

    toDoTask() {
        let taskToDo = {
            description: this.task.description,
            id_user: this.idUserSelect
        }
        this.taskService.postTask(taskToDo)
            .then(taskRespond => {
                this.task = taskRespond;
            })

    }

    listTask(idUser: number) {
        this.taskService.getTasksByidUser(idUser)
            .then(tasksRespond => {
                this.tasks = tasksRespond;
            })
    }


}
