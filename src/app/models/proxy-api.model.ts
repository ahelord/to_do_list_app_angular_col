/**
 * Created by lrodriguez on 6/04/17.
 */
export class ProxyApi{
    public proxy;

    constructor( value: string = 'http://to-do-list-app-angular-col-prod.us-east-1.elasticbeanstalk.com/api/'){
        this.proxy = value;
    }

}
