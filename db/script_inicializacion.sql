CREATE USER angular_col PASSWORD 'angularcol2017';
CREATE USER angularcol PASSWORD 'angularcol';
ALTER ROLE angularcol with SUPERUSER LOGIN;
create database to_do_app_development owner angular_col;
create database to_do_app_test owner angular_col;
create database to_do_app_production owner angularcol;