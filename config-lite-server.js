module.exports = function (bs) {
    var proxy = require('http-proxy-middleware');

    return {
        "port": 4200,
        "files": [
            "./dist/**/*.{html,htm,css,js}"
        ],
        "server": {
            "baseDir": "dist",
            "middleware": {
                1: proxy('/api', {
                    target: 'http://localhost:3000/', changeOrigin: true
// for vhosted sites, changes host header to match to target's host
                })
            }
        }
        ,
        "ui": {
            "port": 4201
        }
    };

};
