# To Do App with Continuous Integration

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0.

## Development server Angular

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

# Run express server

Run `npm run nodemon` for a dev server of express (port 3000)

# Run e2e integration continuous

With the command `npm run test:e2e:integration` e2e tests run, dist files are placed on the test server lite server (a proxy is configured to point to the api directory) and runs the test server API with express, sequelize with The test databases.

# Run BlackBox test ApiRest 

With the command `npm run test:backend:single`  run blackbox test of ApiRest, mocha, chai and supertes, express (test env). send request to expres app.


# Run Development server Angular with proxy config

Run `npm run start:api` for a dev server angular with proxy config.

# Run Express With Pm2
Run `npm run production:reload:apirest`
