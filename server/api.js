/**
 * Created by lrodriguez on 4/10/17.
 */
var express = require('express');
var router = express.Router();

var userRoute = require('./routes/user.route')
var taskRoute = require('./routes/task.route')
var currentDBRoute = require('./routes/current-db.route')
router.use('/user', userRoute)
router.use('/task', taskRoute)
router.use('/current-db', currentDBRoute)


module.exports = router;
