"use strict";

module.exports = {

    up: function (queryInterface, Sequelize) {
        return queryInterface
            .createTable('user', {
                id_user: {
                    type: Sequelize.INTEGER,
                    primaryKey: true,
                    autoIncrement: true,
                    allowNull: false
                },
                email: Sequelize.STRING,
                full_name: Sequelize.STRING,
                created_at: {
                    type: Sequelize.DATE,
                 
                },
                updated_at: Sequelize.DATE
            });
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface
            .dropTable('user');
    }
};
