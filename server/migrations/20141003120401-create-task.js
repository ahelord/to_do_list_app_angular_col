"use strict";

module.exports = {

    up: function (queryInterface, Sequelize) {
        return queryInterface
            .createTable('task', {
                id_task: {
                    type: Sequelize.INTEGER,
                    primaryKey: true,
                    autoIncrement: true,
                    allowNull: false
                },
                id_user: {
                    type: Sequelize.INTEGER,
                    onDelete: "CASCADE",
                    allowNull: false,
                    references: {
                        model: 'user',
                        key: 'id_user'
                    }
                },
                description: Sequelize.STRING,
                created_at: {
                    type: Sequelize.DATE,
                },
                updated_at: Sequelize.DATE,

            });
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface
            .dropTable('task');
    }
};
