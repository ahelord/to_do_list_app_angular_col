/**
 * Created by lrodriguez on 4/26/17.
 */
var sequelize = require('../models/index').sequelize;

module.exports = {

    getCurrentDb(req, res) {
        sequelize.query("select * from current_database()",
            {type: sequelize.QueryTypes.SELECT}
        ).then(function (currentDbRespond) {
            var currentDbObject = currentDbRespond[0]
            res.status(200).json(currentDbObject);
        }).catch(function (error) {
            res.status(500).json(error);
        });
    },

};