/**
 * Created by lrodriguez on 6/4/17.
 */

var task = require('../models/index').task;

module.exports = {
  //Get all task by id user
  getTaskByIdUser(req, res) {
    task.findAll({where: {id_user: req.params.id_user}})
      .then(function (taskRespond) {
        res.status(200).json(taskRespond);
      }).catch(function (error) {
      res.status(500).json(error);
    });
  },
  //Post task
  postTask(req, res) {
    task.create(req.body)
      .then(function (taskRespond) {
        res.status(200).json(taskRespond);
      })
      .catch(function (error) {
        res.status(500).json(error);

      })
  },


};
