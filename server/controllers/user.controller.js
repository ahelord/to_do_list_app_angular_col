/**
 * Created by lrodriguez on 4/10/17.
 */

var user = require('../models/index').user;

module.exports = {
  //Get all users
  getUser(req, res) {
    user.findAll()
      .then(function (usersRespond) {
        res.status(200).json(usersRespond);
      }).catch(function (error) {
      res.status(500).json(error);
    });
  },
  //Post User
  postUser(req, res) {
    user.create(req.body)
      .then(function (userRespond) {
        res.status(200).json(userRespond);
      })
      .catch(function (error) {
        res.status(500).json(error);

      })
  },


};
