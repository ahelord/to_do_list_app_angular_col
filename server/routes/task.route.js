/**
 * Created by lrodriguez on 6/4/17.
 */
var express = require('express');
var router = express.Router();
var taskController = require('../controllers/task.controller')

router.post('/', taskController.postTask);
router.get('/:id_user', taskController.getTaskByIdUser)

module.exports = router;
