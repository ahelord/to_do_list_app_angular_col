/**
 * Created by lrodriguez on 4/26/17.
 */
var express = require('express');
var router = express.Router();
var currentDbController = require('../controllers/current-db.controller');

router.get('/', currentDbController.getCurrentDb);

module.exports = router;