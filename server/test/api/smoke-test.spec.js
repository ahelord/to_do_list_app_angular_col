/**
 * Created by lrodriguez on 3/16/17.
 */

var server = require("../../server")
var serverStart = require("../../start_server")
var sequelize = require('../../models/index').sequelize

/*For Test*/
var chai = require('chai');
var request = require('supertest');
var expect = chai.expect;
var assert = require('assert');

/*For test*/

var objToday = new Date(),
    weekday = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'),
    dayOfWeek = weekday[objToday.getDay()],
    domEnder = function () {
        var a = objToday;
        if (/1/.test(parseInt((a + "").charAt(0)))) return "th";
        a = parseInt((a + "").charAt(1));
        return 1 == a ? "st" : 2 == a ? "nd" : 3 == a ? "rd" : "th"
    }(),
    dayOfMonth = today + ( objToday.getDate() < 10) ? '0' + objToday.getDate() + domEnder : objToday.getDate() + domEnder,
    months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'),
    curMonth = months[objToday.getMonth()],
    curYear = objToday.getFullYear(),
    curHour = objToday.getHours() > 12 ? objToday.getHours() - 12 : (objToday.getHours() < 10 ? "0" + objToday.getHours() : objToday.getHours()),
    curMinute = objToday.getMinutes() < 10 ? "0" + objToday.getMinutes() : objToday.getMinutes(),
    curSeconds = objToday.getSeconds() < 10 ? "0" + objToday.getSeconds() : objToday.getSeconds(),
    curMeridiem = objToday.getHours() > 12 ? "PM" : "AM";
var today = curHour + ":" + curMinute + "." + curSeconds + curMeridiem + " " + dayOfWeek + " " + dayOfMonth + " of " + curMonth + ", " + curYear;


describe('Smoke Test - Pruebas Caja Negra ', function () {
    var userCreated = ''
    var taskCreated = ''
    before(function () {

        sequelize.sync()
        // runs before all tests in this block
    });

    after(function () {

        serverStart.server.close()
        userCreated = ''
        taskCreated = ''

        // runs after all tests in this block
    });

    beforeEach(function () {


        // runs before each test in this block
    });

    afterEach(function () {

        // runs after each test in this block

    });

    describe('# Mocha opera', function () {
        it('Puedo afirmar (Assert)', function () {
            assert.equal(true, true);
        });

    });
    describe('# La base de datos de prueba opera', function () {
        it('La base de datos actual es la base de datos de pruebas', function () {
            // newer mocha versions accepts promises as well
            return request(server)
                .get('/api/current-db/')
                .set('Accept', 'application/json')
                .expect(200, {
                    current_database: 'to_do_app_test'
                })
        })
    });


    describe('# Prueba API endpoint user', function () {


        it("Puedo publicar un usuario ", function (done) {

            request(server)
                .post('/api/user')
                .set('Content-Type', 'application/json')
                .send({
                    full_name: 'Leonardo Rodriguez',
                    email: 'test' + curHour + curMinute + '@angular.co'
                })
                .expect(200, function (err, result) {
                    expect(result.statusCode).to.equal(200);
                    expect(result.body).to.have.property('id_user')
                    expect(result.body).to.have.property('email')
                    userCreated = result.body
                    done();
                });

        });


        it("Puedo obtener los usuarios", function (done) {

            request(server)
                .get('/api/user/')
                .expect(200, function (err, result) {
                    expect(result.statusCode).to.equal(200);
                    expect(result.body[0]).to.have.property('id_user')
                    done();
                });

        });

    })

    describe('# Prueba API endpoint Task', function () {
        it("Puedo publicar una tarea ", function (done) {

            request(server)
                .post('/api/task')
                .set('Content-Type', 'application/json')
                .send({
                    id_user: userCreated.id_user,
                    description: 'Hacer Footer de la pagina'
                })
                .expect(200, function (err, result) {
                    expect(result.statusCode).to.equal(200);
                    expect(result.body).to.have.property('id_task')
                    expect(result.body).to.have.property('description')
                    taskCreated = result.body
                    done();
                });

        });
        it("Puedo obtener las tareas por el id_user", function (done) {

            request(server)
                .get('/api/task/' + userCreated.id_user)
                .expect(200, function (err, result) {
                    expect(result.statusCode).to.equal(200);
                    expect(result.body[0].id_user).to.equal(userCreated.id_user);
                    done();
                });

        });
    })


})
;
