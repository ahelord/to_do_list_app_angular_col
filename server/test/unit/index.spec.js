'use strict';

var expect = require('expect.js');

describe('models/index', function () {
  it('Retorna el modelo Tarea', function () {
    var task = require('../../models').task;
    expect(task).to.be.ok();
  });

  it('Retorna el modelo Usuario', function () {
    var user = require('../../models').user;
    expect(user).to.be.ok();
  });
});
