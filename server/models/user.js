"use strict";

module.exports = function (sequelize, DataTypes) {
  var User = sequelize.define("user", {
    id_user: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    email: {
      type: DataTypes.STRING,
    },
    full_name: {
      type: DataTypes.STRING,
    },
  }, {
    tableName: 'user',
    freezeTableName: true,
    underscored: true,
    classMethods: {
      associate: function (models) {
        // associations can be defined here
      }
    }

  });

  return User;
};
