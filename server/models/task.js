"use strict";

module.exports = function (sequelize, DataTypes) {
    var Task = sequelize.define("task", {
        id_task: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        id_user: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'user',
                key: 'id_user'
            }
        },
        description: {
            type: DataTypes.STRING,
        },

    }, {
        tableName: 'task',
        freezeTableName: true,
        underscored: true,
        classMethods: {
            associate: function (models) {
                // associations can be defined here
            }
        }

    });

    return Task;
};
