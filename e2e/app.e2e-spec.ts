import {ToDoLisAppAngularCol} from './app.po';

describe('To-Do List App - Angular Col', () => {
    let page: ToDoLisAppAngularCol;

    beforeEach(() => {
        page = new ToDoLisAppAngularCol();
    });

    it('Puedo ver el titulo principal de la pagina:', () => {
        page.navigateTo();
        expect(page.getParagraphText()).toEqual('To-Do List App');
    });
});
